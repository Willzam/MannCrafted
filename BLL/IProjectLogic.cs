﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IProjectLogic
    {
        List<ProjectSM> GetProjects();
        void CreateProject(ProjectSM item);
        ProjectSM GetProjectById(int tempId);
        void DeleteProjectById(int Id);
        void UpdateProjectById(ProjectSM item);
    }
}
