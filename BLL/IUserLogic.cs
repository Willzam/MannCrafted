﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IUserLogic
    {
        List<UserSM> GetUsers();
        bool CheckUsername(string tempUsername);
        UserSM GetUserById(int tempId);
        bool Login(UserSM tempUser);
        void CreateUser(UserSM user);
        void DeleteUserById(int id);
        void UpdateUserById(UserSM user);
        UserSM GetUser(UserSM user);
        void UpdatePassword(PasswordSM pass);
    }
}
