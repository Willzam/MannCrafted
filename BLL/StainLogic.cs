﻿using ApplicationLogger;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class StainLogic
    {
        private IStainDAO stainData;
        private static ILoggerIO logs;

        public StainLogic(IStainDAO stainDAO,ISQLDAO dao, ILoggerIO log)
        {
            logs = log;
            stainData = stainDAO;   //injecting dependency
            stainData.GetDataWriter(dao);//dependency injector through a infrastructure
        }
        public List<StainSM> GetStains()
        {
            return Map(stainData.GetStains());
        }
       
        
        private StainSM Map(StainDM item)
        {
            StainSM stuff = new StainSM();
            stuff.Id = Convert.ToInt32(item.Id);
            stuff.Stain= item.Stain;
            return stuff;
        }
        private StainDM Map(StainSM item)
        {
            StainDM stuff = new StainDM();
            stuff.Id = item.Id.ToString();
            stuff.Stain = item.Stain;
            return stuff;
        }
        private List<StainSM> Map(List<StainDM> stain)
        {
            List<StainSM> items = new List<StainSM>();
            foreach (StainDM item in stain)
            {
                items.Add(Map(item));
            }
            return items;
        }
        private List<StainDM> Map(List<StainSM> stain)
        {
            List<StainDM> items = new List<StainDM>();
            foreach (StainSM item in stain)
            {
                items.Add(Map(item));
            }
            return items;
        }
        public void CreateStain(StainSM item)
        {
            try
            {
                stainData.CreateStain(Map(item));
                logs.LogError("Event ", "User was able to create new item ", "Class:StainLogic, Method:CreateStain");
            }
            catch (Exception P)
            {
                logs.LogError("Error ", "User was unable to create a new item ", "Class:StainLogic, Method:CreateStain");
            }
        }
        
        public StainSM GetStainById(int tempId) 
        {
            return Map(stainData.GetStainById(tempId.ToString()));
        }

        public void DeleteStainById(int Id)
        {
            try
            {
                stainData.DeleteStainById(Id.ToString());
                logs.LogError("Event ", "User was able to Delete item", "Class:StainLogic, Method:DeleteStainById");
            }
            catch (Exception z)
            {
                logs.LogError("Error ", "User was unable to Delete item", "Class:StainLogic, Method:DeleteStainById");
            }
        }
        public void UpdateStainById(StainSM item)
        {
            try
            {
                stainData.UpdateStainById(Map(item));
                logs.LogError("Event ", "User was able to update item", "Class:StainLogic, Method:UpdateStainById");
            }
            catch(Exception j)
            {
                logs.LogError("Error ", "User was unable to update a item", "Class:StainLogic, Method:UpdateStainById");
            }
        }
    }
}
