﻿using ApplicationLogger;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL
{
    public class UserLogic:IUserLogic
    {
        private ILoggerIO logs;
        private IUserDAO userData;
        private IHashing hash;

        public UserLogic(IUserDAO userDAO, ISQLDAO dao, ILoggerIO log, IHashing hasher)
        {
            hash = hasher;
            logs = log;
            userData = userDAO;   //injecting dependency
            userData.GetDataWriter(dao);//dependency injector through a infrastructure
        }
        public List<UserSM> GetUsers() //Creates UserList
        {
            return Map(userData.GetUsers());
        }
        public UserSM Map(UserDM human) // Converts for user in the Logic/Presentation Layer
        {
            UserSM hm = new UserSM();
            hm.UserName = human.UserName;
            hm.Password = human.Password;
            hm.Role = human.Role;
            hm.Name = human.Name;
            hm.Address = human.Address;
            hm.City = human.City;
            hm.State = human.State;
            hm.Zip = human.Zip;
            hm.Phone = human.Phone;
            hm.Id = Convert.ToInt32(human.Id);
            return hm;
        }
        public UserDM Map(UserSM human) //Converts for use in the Data Layer
        {
            UserDM hm = new UserDM();
            hm.UserName = human.UserName;
            hm.Password = human.Password;
            hm.Role = human.Role;
            hm.Name = human.Name;
            hm.Address = human.Address;
            hm.City = human.City;
            hm.State = human.State;
            hm.Zip = human.Zip;
            hm.Phone = human.Phone;
            hm.Id = human.Id.ToString();
            return hm;
        }
        public List<UserSM> Map(List<UserDM> users) //Converts for Use in the Logic/Presentation Layer
        {
            List<UserSM> people = new List<UserSM>();
            foreach (UserDM user in users)
            {
                people.Add(Map(user));
            }
            return people;
        }
        public List<UserDM> Map(List<UserSM> users) //Converts for use in the Data Layer
        {
            List<UserDM> people = new List<UserDM>();
            foreach (UserSM user in users)
            {
                people.Add(Map(user));
            }
            return people;
        }
        public bool CheckUsername(string tempUsername) //Matches UserName Against UserList
        {
            List<UserSM> list = GetUsers();
            foreach (UserSM item in list)
            {
                if (tempUsername == item.UserName)
                {
                    return true;
                }
            }
            return false;
        }
        public UserSM GetUserById(int tempId) //Finds a User in the UserList by Id
        {
            return Map(userData.GetUserById(tempId.ToString()));
        }
        public bool Login(UserSM tempUser)
        {
            try
            {
                if (tempUser.Id > 0)
                {
                    UserSM user = GetUserById(tempUser.Id);

                    if (tempUser.Password == user.Password)
                    {
                        logs.LogError("Event", "User was successfully able to Login", "Class:UserLogic, Method::Login");
                        return true;
                    }
                }
            }
            catch (Exception a)
            {
                logs.LogError("Error", "User was unable to Login", "Class:UserLogic, Method:Login");
            }
            return false;
        }
        public void CreateUser(UserSM user)
        {
            try
            {
                if (!CheckUsername(user.UserName))
                {
                    user.Password = hash.GetHash(user.Password);
                   // user.Role = "User";
                    userData.CreateUser(Map(user));

                    logs.LogError("Event", "a new user has been been added to database", "Class:UserLogic,Method:CreateUser");
                }
            }
            catch (Exception b)
            {
                logs.LogError("Error", "A new user has not been added to the database", "Class:UserLogic,Method:CreateUser");
            }
        }
        public void UpdateUserById(UserSM user)
        {
            try
            {
                userData.UpdateUserById(Map(user));
                logs.LogError("Event", "User was successfully able to update", "Class:UserLogic, Method:UpdateUser");
            }
            catch (Exception c)
            {
                logs.LogError("Error", "User was unable to update", "Class:UserLogic, Method:UpdateUser");
            }
        }
        public void DeleteUserById(int id)
        {
            try
            {
                userData.DeleteUserById(id);
                logs.LogError("Event ", "User was able to Delete a User ", "Class:UserLogic, Method:DeleteUserById");
            }
            catch (Exception d)
            {
                logs.LogError("Error ", "User was unable to Delete a User ", "Class:UserLogic, Method:DeleteUserById");
            }
        }
        public UserSM GetUser(UserSM user)
        {
            user.Password = hash.GetHash(user.Password);
            return Map(userData.GetUser(Map(user)));
        }
        public void UpdatePassword(PasswordSM pass)
        {
            UserSM user = GetUserById(pass.Id);
            if (pass.NewPassword == pass.ConfirmPassword)
            {
                if (hash.GetHash(pass.OldPassword) == user.Password)
                {
                    user.Password = hash.GetHash(pass.NewPassword);
                    userData.UpdateUserById(Map(user));
                }
            }
        }
    }
}
