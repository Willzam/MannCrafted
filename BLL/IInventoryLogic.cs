﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IInventoryLogic
    {
        List<InventorySM> GetInventory();
        InventorySM GetInventoryById(int id);
        void DeleteInventoryById(int Id);
        void UpdateInventoryById(InventorySM item);
        void CreateInventory(InventorySM item);
    }
}
