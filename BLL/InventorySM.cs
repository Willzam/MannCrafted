﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class InventorySM
    {
        public int Id { get; set; }
        public string Project { get; set; }
        public string Stain { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}
