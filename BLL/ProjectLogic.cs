﻿using ApplicationLogger;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ProjectLogic:IProjectLogic
    {
        private IProjectDAO projectData;
        private static ILoggerIO logs;

        public ProjectLogic(IProjectDAO projectDAO,ISQLDAO dao, ILoggerIO log)
        {
            logs = log;
            projectData = projectDAO;   //injecting dependency
            projectData.GetDataWriter(dao);//dependency injector through a infrastructure
        }
        public List<ProjectSM> GetProjects()
        {
            return Map(projectData.GetProjects());
        }
       
        
        private ProjectSM Map(ProjectDM item)
        {
            ProjectSM stuff = new ProjectSM();
            stuff.Id = Convert.ToInt32(item.Id);
            stuff.Project= item.Project;
            return stuff;
        }
        private ProjectDM Map(ProjectSM item)
        {
            ProjectDM stuff = new ProjectDM();
            stuff.Id = item.Id.ToString();
            stuff.Project = item.Project;
            return stuff;
        }
        private List<ProjectSM> Map(List<ProjectDM> project)
        {
            List<ProjectSM> items = new List<ProjectSM>();
            foreach (ProjectDM item in project)
            {
                items.Add(Map(item));
            }
            return items;
        }
        private List<ProjectDM> Map(List<ProjectSM> project)
        {
            List<ProjectDM> items = new List<ProjectDM>();
            foreach (ProjectSM item in project)
            {
                items.Add(Map(item));
            }
            return items;
        }
        public void CreateProject(ProjectSM item)
        {
            try
            {
                projectData.CreateProject(Map(item));
                logs.LogError("Event ", "User was able to create new item ", "Class:ProjectLogic, Method:CreateProject");
            }
            catch (Exception P)
            {
                logs.LogError("Error ", "User was unable to create a new item ", "Class:ProjectLogic, Method:CreateProject");
            }
        }
        
        public ProjectSM GetProjectById(int tempId) 
        {
            return Map(projectData.GetProjectById(tempId.ToString()));
        }

        public void DeleteProjectById(int Id)
        {
            try
            {
                projectData.DeleteProjectById(Id);
                logs.LogError("Event ", "User was able to Delete item", "Class:ProjectLogic, Method:DeleteProjectById");
            }
            catch (Exception z)
            {
                logs.LogError("Error ", "User was unable to Delete item", "Class:ProjectLogic, Method:DeleteProjectById");
            }
        }
        public void UpdateProjectById(ProjectSM item)
        {
            try
            {
                projectData.UpdateProjectById(Map(item));
                logs.LogError("Event ", "User was able to update item", "Class:ProjectLogic, Method:UpdateProjectById");
            }
            catch(Exception j)
            {
                logs.LogError("Error ", "User was unable to update a item", "Class:ProjectLogic, Method:UpdateProjectById");
            }
        }
    }
}
