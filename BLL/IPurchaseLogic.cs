﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IPurchaseLogic
    {
        List<PurchaseSM> GetPurchase();
        PurchaseSM GetPurchaseById(int id);
        void CheckOut(InventorySM item, int id);
        decimal CalcTotalPurchase(InventorySM item);
        void UpdatePurchaseById(PurchaseSM item);
        void DeletePurchaseById(int Id);
    }
}
