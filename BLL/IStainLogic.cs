﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IStainLogic
    {
        List<StainSM> GetStains();
        StainSM GetStainById(int tempId);
        void CreateStain(StainSM user);
        void DeleteStainById(int id);
        void UpdateStain(StainSM user);
    }
}
