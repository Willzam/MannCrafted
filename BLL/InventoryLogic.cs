﻿using ApplicationLogger;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class InventoryLogic
    {
        private IInventoryDAO itemData;
        private static ILoggerIO logs;

        public InventoryLogic(IInventoryDAO inventoryDAO,ISQLDAO dao, ILoggerIO log)
        {
            logs = log;
            itemData = inventoryDAO;   //injecting dependency
            itemData.GetDataWriter(dao);//dependency injector through a infrastructure
        }
        public List<InventorySM> GetInventory()
        {
            return Map(itemData.GetInventory());
        }
       
        
        private InventorySM Map(InventoryDM item)
        {
            InventorySM stuff = new InventorySM();
            stuff.Id = Convert.ToInt32(item.Id);
            stuff.Project= item.Project;
            stuff.Stain = item.Stain;
            stuff.Price = Convert.ToDecimal(item.Price);
            stuff.Quantity= Convert.ToInt32(item.Quantity);
            return stuff;
        }
        private InventoryDM Map(InventorySM item)
        {
            InventoryDM stuff = new InventoryDM();
            stuff.Id = item.Id.ToString();
            stuff.Project = item.Project;
            stuff.Stain = item.Stain;
            stuff.Price = item.Price.ToString();
            stuff.Quantity = item.Quantity.ToString();
            return stuff;
        }
        private List<InventorySM> Map(List<InventoryDM> Inventory)
        {
            List<InventorySM> items = new List<InventorySM>();
            foreach (InventoryDM item in Inventory)
            {
                items.Add(Map(item));
            }
            return items;
        }
        private List<InventoryDM> Map(List<InventorySM> Inventory)
        {
            List<InventoryDM> items = new List<InventoryDM>();
            foreach (InventorySM item in Inventory)
            {
                items.Add(Map(item));
            }
            return items;
        }
        public void CreateInventory(InventorySM item)
        {
            try
            {
                itemData.CreateInventory(Map(item));
                logs.LogError("Event ", "User was able to create new item ", "Class:InventoryLogic, Method:CreateInventory");
            }
            catch (Exception P)
            {
                logs.LogError("Error ", "User was unable to create a new item ", "Class:InventoryLogic, Method:CreateInventory");
            }
        }
        
        public InventorySM GetInventoryById(int tempId) //Finds a Item in the Inventory
        {
            return Map(itemData.GetInventoryById(tempId.ToString()));
        }

        public void DeleteInventoryById(int Id)
        {
            try
            {
                itemData.DeleteInventoryById(Id.ToString());
                logs.LogError("Event ", "User was able to delete item", "Class:InventoryLogic, Method:DeleteInventoryById");
            }
            catch (Exception z)
            {
                logs.LogError("Error ", "User was unable to delete item", "Class:InventoryLogic, Method:DeleteInventoryById");
            }
        }
        public void UpdateInventoryById(InventorySM item)
        {
            try
            {
                itemData.UpdateInventoryById(Map(item));
                logs.LogError("Event ", "User was able to update item", "Class:InventoryLogic, Method:UpdateInventoryById");
            }
            catch(Exception j)
            {
                logs.LogError("Error ", "User was unable to update a item", "Class:InventoryLogic, Method:UpdateInventoryById");
            }
        }
    }
}
