﻿using ApplicationLogger;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PurchaseLogic: IPurchaseLogic
    {
        private IPurchaseDAO purchaseData;
        private static ILoggerIO logs;

        public PurchaseLogic(IPurchaseDAO purchaseDAO, ISQLDAO dao, ILoggerIO log)
        {
            logs = log;
            purchaseData = purchaseDAO;   //injecting dependency
            purchaseData.GetDataWriter(dao);//dependency injector through a infrastructure
        }

        public List<PurchaseSM> GetPurchase()
        {
            return Map(purchaseData.GetPurchase());
        }
        private InventorySM Map(InventoryDM item)
        {
            InventorySM stuff = new InventorySM();
            stuff.Id = Convert.ToInt32(item.Id);
            stuff.Project = item.Project;
            stuff.Stain = item.Stain;
            stuff.Price = Convert.ToDecimal(item.Price);
            stuff.Quantity = Convert.ToInt32(item.Quantity);
            return stuff;
        }
        private InventoryDM Map(InventorySM item)
        {
            InventoryDM stuff = new InventoryDM();
            stuff.Id = item.Id.ToString();
            stuff.Project = item.Project;
            stuff.Stain = item.Stain;
            stuff.Price = item.Price.ToString();
            stuff.Quantity = item.Quantity.ToString();
            return stuff;
        }
        private List<InventorySM> Map(List<InventoryDM> Inventory)
        {
            List<InventorySM> items = new List<InventorySM>();
            foreach (InventoryDM item in Inventory)
            {
                items.Add(Map(item));
            }
            return items;
        }
        private List<InventoryDM> Map(List<InventorySM> Inventory)
        {
            List<InventoryDM> items = new List<InventoryDM>();
            foreach (InventorySM item in Inventory)
            {
                items.Add(Map(item));
            }
            return items;
        }
        private PurchaseSM Map(PurchaseDM purchase)
        {
            PurchaseSM stuff = new PurchaseSM();
            stuff.Id = Convert.ToInt32(purchase.Id);
            stuff.Total = Convert.ToDecimal(purchase.Total);
            stuff.Project = purchase.Project;
            stuff.Stain = purchase.Stain;
            stuff.Quantity = Convert.ToInt32(purchase.Quantity);
            stuff.UserName = purchase.UserName;
            stuff.Email = purchase.Email;
            stuff.Phone = purchase.Phone;
            stuff.Name = purchase.Name;
            stuff.Address = purchase.Address;
            stuff.City = purchase.City;
            stuff.State = purchase.State;
            stuff.Zip = purchase.Zip;
            return stuff;
        }
        private PurchaseDM Map(PurchaseSM purchase)
        {
            PurchaseDM stuff = new PurchaseDM();
            stuff.Id = purchase.Id.ToString();
            stuff.Total = purchase.Total.ToString();
            stuff.Stain = purchase.Stain;
            stuff.Project = purchase.Project;
            stuff.Quantity = purchase.Quantity.ToString();
            stuff.UserName = purchase.UserName;
            stuff.Email = purchase.Email;
            stuff.Phone = purchase.Phone;
            stuff.Name = purchase.Name;
            stuff.Address = purchase.Address;
            stuff.City = purchase.City;
            stuff.State = purchase.State;
            stuff.Zip = purchase.Zip;
            return stuff;
        }
        private List<PurchaseSM> Map(List<PurchaseDM> Inventory)
        {
            List<PurchaseSM> purchases = new List<PurchaseSM>();
            foreach (PurchaseDM purchase in Inventory)
            {
                purchases.Add(Map(purchase));
            }
            return purchases;
        }
        private List<PurchaseDM> Map(List<PurchaseSM> Inventory)
        {
            List<PurchaseDM> purchases = new List<PurchaseDM>();
            foreach (PurchaseSM purchase in Inventory)
            {
                purchases.Add(Map(purchase));
            }
            return purchases;
        }
        public PurchaseSM GetPurchaseById(int id)
        {
            return Map(purchaseData.GetPurchaseById(id));
        }
        public void CheckOut(InventorySM item, int id)
        {
            purchaseData.CheckOut(Map(item), id.ToString(), CalcTotalPurchase(item).ToString());
        }

        public decimal CalcTotalPurchase(InventorySM item)
        {
            decimal total = 0;
            total += item.Price + (item.Price * Convert.ToDecimal(0.04));
            return total;
        }

        public void DeletePurchaseById(int Id)
        {
            try
            {
                purchaseData.DeletePurchaseById(Id.ToString());
                logs.LogError("Event ", "User was able to delete a purchase", "Class:PurchaseLogic, Method:DeletePurchaseById");
            }
            catch (Exception Ucd)
            {
                logs.LogError("Error ", "User was unable to delete a purchase", "Class:PurchaseLogic, Method:DeletePurchaseById");
            }
        }
        public void UpdatePurchaseById(PurchaseSM item)
        {
            try
            {
                purchaseData.UpdatePurchaseById(Map(item));
                logs.LogError("Event ", "User was able to update purchase", "Class:PurchaseLogic, Method:UpdateInventoryById");
            }
            catch (Exception e)
            {
                logs.LogError("Error ", "User was unable to update a purchase", "Class:PurchaseLogic, Method:UpdateInventoryById");
            }
        }
    }
}
