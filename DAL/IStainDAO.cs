﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IStainDAO
    {
        void GetDataWriter(ISQLDAO dataWriter);
        List<StainDM> Read(SqlParameter[] parameters, string statement);
        List<StainDM> GetStains();
        void CreateStain(StainDM stain);
        void DeleteStainById(string id);
        void UpdateStainById(StainDM stain);
        StainDM GetStainById(string id);
    }
}
