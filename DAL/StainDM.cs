﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class StainDM
    {
        public string Id { get; set; }
        public string Stain { get; set; }
    }
}
