﻿using ApplicationLogger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class InventoryDAO
    {
        private ILoggerIO logs = new LoggerIO();
        private ISQLDAO dataWriter = new SQLDAO();
        private string connectionString = @"Server=.\SQLEXPRESS;Database=MannCrafted;Trusted_Connection=true;";

         public InventoryDAO(ILoggerIO log)
        {
            logs = log;
        }
        public void GetDataWriter(ISQLDAO dataWriter)
        {
            this.dataWriter = dataWriter;
        }
        public List<InventoryDM> Read(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);

                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<InventoryDM> products = new List<InventoryDM>();
                    while (data.Read())
                    {
                        InventoryDM product = new InventoryDM();
                        product.Id = data["ID"].ToString();
                        product.Project = data["Project"].ToString();
                        product.Stain= data["Stain"].ToString();
                        product.Price = data["Price"].ToString();
                        product.Quantity = data["Quantity"].ToString();
                        products.Add(product);
                    }
                    return products;
                }
            }
        }
        public List<InventoryDM> GetInventory()
        {
            return Read(null, "GetInventory");
        }
       
        public void CreateInventory(InventoryDM product)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                 new SqlParameter("@Project", product.Project)
                ,new SqlParameter("@Price", product.Price)
                ,new SqlParameter("@Quantity", product.Quantity)
                ,new SqlParameter("@Stain", product.Stain)
            };
            dataWriter.Write(parameters, "CreateInventory");
        }
        public void DeleteInventoryById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            dataWriter.Write(parameters, "DeleteInventoryById");
        }
        
        public void UpdateInventoryById(InventoryDM product)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@ID", product.Id)
                ,new SqlParameter("@NewProject",product.Project)
                ,new SqlParameter("@NewPrice",product.Price)
                ,new SqlParameter("@NewQuantity",product.Quantity)
                ,new SqlParameter("@NewStain" , product.Stain)
            };
            dataWriter.Write(parameters, "UpdateInventoryById");
        }
        
        public InventoryDM GetInventoryById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            return Read(parameters, "GetInventoryById").SingleOrDefault();
        }

    }
}
