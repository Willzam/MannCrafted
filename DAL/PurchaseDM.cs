﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PurchaseDM
    {
        public string Id {get; set;}
        public string Total { get; set; }
        public string Project {get; set;}
        public string Stain {get; set;}
        public string Quantity {get; set;}
        public string UserName {get; set;}
        public string Email {get; set;}
        public string Phone {get; set;}
        public string Name {get; set;}
        public string Address {get; set;}
        public string City {get; set;}
        public string State {get; set;}
        public string Zip {get; set;}
    }
}
