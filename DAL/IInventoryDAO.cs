﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IInventoryDAO
    {
        List<InventoryDM> Read(SqlParameter[] parameters, string statement);
        List<InventoryDM> GetInventory();
        void DeleteInventoryById(string id);
        void UpdateInventoryById(InventoryDM product);
        InventoryDM GetInventoryById(string id);
        void GetDataWriter(ISQLDAO dataWriter);
        void CreateInventory(InventoryDM product);
    }
}
