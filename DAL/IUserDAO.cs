﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUserDAO
    {
        List<UserDM> Read(SqlParameter[] parameters, string statement);
        List<UserDM> GetUsers();
        void DeleteUserById(int Id);
        void UpdateUserById(UserDM user);
        void CreateUser(UserDM user);
        UserDM GetUserById(string id);
        void GetDataWriter(ISQLDAO dataWriter);
        UserDM GetUser(UserDM veriUser);
    }
}
