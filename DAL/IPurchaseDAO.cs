﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IPurchaseDAO
    {
        List<PurchaseDM> Read(SqlParameter[] parameters, string statement);
        void GetDataWriter(ISQLDAO dataWriter);
        List<PurchaseDM> GetPurchase();
        void CheckOut(InventoryDM product, string id, string total);
        PurchaseDM GetPurchaseById(int id);
        void DeletePurchaseById(string id);
        void UpdatePurchaseById(PurchaseDM product);
    }
}
