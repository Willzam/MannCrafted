﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IProjectDAO
    {
        void GetDataWriter(ISQLDAO dataWriter);
        List<ProjectDM> Read(SqlParameter[] parameters, string statement);
        List<ProjectDM> GetProjects();
        void CreateProject(ProjectDM project);
        void DeleteProjectById(int id);
        void UpdateProjectById(ProjectDM project);
        ProjectDM GetProjectById(string id);

    }
}
