﻿using ApplicationLogger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class StainDAO:IStainDAO
    {
        private ISQLDAO dataWriter;
        private ILoggerIO logs;
        private string connectionString = @"Server=.\SQLEXPRESS;Database = MannCrafted;Trusted_Connection=True;";

        public StainDAO(ILoggerIO log)
        {
            logs = log;
        }
        public void GetDataWriter(ISQLDAO dataWriter)
        {
            this.dataWriter = dataWriter;
        }
        public List<StainDM> Read(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<StainDM> stains = new List<StainDM>();
                    while (data.Read())
                    {
                        StainDM stain = new StainDM();
                        stain.Id = data["Id"].ToString();
                        stain.Stain = data["Stain"].ToString();
                        stains.Add(stain);
                    }
                    return stains;
                }
            }
        }
        public List<StainDM> GetStains()
        {
            return Read(null, "GetStains");
        }

        public void CreateStain(StainDM stain)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Stain", stain.Stain)
            };
            dataWriter.Write(parameters, "CreateStain");
            logs.LogError("Event", "An Stain has been added to the database", "Class:StainDAO, Method:CreateStain");
        }

        public void DeleteStainById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            dataWriter.Write(parameters, "DeleteStainById");
            logs.LogError("Event", "An Stain has been removed from the database", "Class:StainDAO, Method:DeleteStainById");

        }

        public void UpdateStainById(StainDM stain)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@NewStain", stain.Stain)
                , new SqlParameter("@Id", stain.Id) 
            };
            dataWriter.Write(parameters, "UpdateStainById");
            logs.LogError("Event", "An Stain has been updated", "Class:StainDAO, Method: UpdateStainById");
        }

        public StainDM GetStainById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            return Read(parameters, "GetStainById").SingleOrDefault();
        }

    }
}
