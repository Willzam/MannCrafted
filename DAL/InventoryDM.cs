﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class InventoryDM
    {
        public string Id { get; set; }
        public string Project { get; set; }
        public string Stain { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
    }
}
