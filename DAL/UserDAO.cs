﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationLogger;
using System.Data;

namespace DAL
{
    public class UserDAO:IUserDAO
    {
        private ISQLDAO dataWriter;
        private ILoggerIO logs;
        private string connectionString = @"Server=.\SQLEXPRESS;Database = MannCrafted;Trusted_Connection=True;";

        public UserDAO(ILoggerIO log)
        {
            logs = log;
        }
        public void GetDataWriter(ISQLDAO dataWriter)
        {
            this.dataWriter = dataWriter;
        }
        public List<UserDM> Read(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<UserDM> people = new List<UserDM>();
                    while (data.Read())
                    {
                        UserDM user = new UserDM();
                        user.Id = data["Id"].ToString();
                        user.UserName = data["UserName"].ToString();
                        user.Password = data["Password"].ToString();
                        user.Email = data["Email"].ToString();
                        user.Role = data["Role"].ToString();
                        user.Name = data["Name"].ToString();
                        user.Address = data["Address"].ToString();
                        user.City = data["City"].ToString();
                        user.State = data["State"].ToString();
                        user.Zip = data["Zip"].ToString();
                        user.Phone = data["Phone"].ToString();
                        people.Add(user);
                    }
                    return people;
                }
            }
        }
        public List<UserDM> GetUsers()
        {
            return Read(null, "GetUsers");
        }

        public void CreateUser(UserDM user)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@UserName", user.UserName)
                ,new SqlParameter("@Password", user.Password)
                ,new SqlParameter("@Email", user.Email)
                ,new SqlParameter("@Role", user.Role)
                ,new SqlParameter("@Name", user.Name)
                ,new SqlParameter("@Address", user.Address)
                ,new SqlParameter("@City", user.City)
                , new SqlParameter("@State", user.State)
                , new SqlParameter("@Zip", user.Zip)
                ,new SqlParameter("@Phone", user.Phone)

            };
            dataWriter.Write(parameters, "CreateUser");
            logs.LogError("Event", "A User has been added to the database", "Class:UserDAO, Method:CreateUser");
        }

        public void DeleteUserById(int id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            dataWriter.Write(parameters, "DeleteUserById");
            logs.LogError("Event", "A User has been deleted from the database", "Class:UserDAO, Method:DeleteUserById");

        }

        public void UpdateUserById(UserDM user)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@NewUserName", user.UserName)
                , new SqlParameter("@NewPassword", user.Password)
                , new SqlParameter("@NewEmail", user.Email)
                ,new SqlParameter("@NewRole", user.Role)
                ,new SqlParameter("@NewName", user.Name)
                ,new SqlParameter("@NewAddress", user.Address)
                ,new SqlParameter("@NewCity", user.City)
                , new SqlParameter("@NewState", user.State)
                , new SqlParameter("@NewZip", user.Zip)
                ,new SqlParameter("@NewPhone", user.Phone)
                , new SqlParameter("@Id", user.Id) 
            };
            dataWriter.Write(parameters, "UpdateUserById");
            logs.LogError("Event", "An User has been updated", "Class:UserDAO, Method: UpdateUserById");
        }

        public UserDM GetUserById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            return Read(parameters, "GetUserById").SingleOrDefault();
        }

        public UserDM GetUser(UserDM veriUser) //May not need this method
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@UserName", veriUser.UserName)
                ,new SqlParameter("@Password", veriUser.Password)
            };
            return Read(parameters, "GetUser").SingleOrDefault();
        }
    }
}
