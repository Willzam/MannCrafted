﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ProjectDM
    {
        public string Id { get; set; }
        public string Project { get; set; }
    }
}
