﻿using ApplicationLogger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PurchaseDAO:IPurchaseDAO
    {
        private ILoggerIO logs = new LoggerIO();
        private ISQLDAO dataWriter = new SQLDAO();
        private string connectionString = @"Server=.\SQLEXPRESS;Database=MannCrafted;Trusted_Connection=true;";

        public PurchaseDAO(ILoggerIO log)
        {
            logs = log;
        }
        public void GetDataWriter(ISQLDAO dataWriter)
        {
            this.dataWriter = dataWriter;
        }
        public List<PurchaseDM> Read(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);

                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<PurchaseDM> products = new List<PurchaseDM>();
                    while (data.Read())
                    {
                        PurchaseDM product = new PurchaseDM();
                        product.Id = data["ID"].ToString();
                        product.Total = data["Total"].ToString();
                        product.Project = data["Project"].ToString();
                        product.Stain = data["Stain"].ToString();
                        product.Quantity = data["Quantity"].ToString();
                        product.UserName = data["UserName"].ToString();
                        product.Email = data["Email"].ToString();
                        product.Phone = data["Phone"].ToString();
                        product.Name = data["Name"].ToString();
                        product.Address = data["Address"].ToString();
                        product.City = data["City"].ToString();
                        product.State = data["State"].ToString();
                        product.Zip = data["Zip"].ToString();
                        
                        products.Add(product);
                    }
                    return products;
                }
            }
        }

        public void DeletePurchaseById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            dataWriter.Write(parameters, "DeletePurchaseById");
        }

        public void UpdatePurchaseById(PurchaseDM product)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@NewTotal",product.Total)
                ,new SqlParameter("@NewStain",product.Stain)
                ,new SqlParameter("@NewProject",product.Project)
                ,new SqlParameter("@NewQuantity",product.Quantity)
                ,new SqlParameter("@NewUserName", product.UserName)
                ,new SqlParameter("@NewEmail", product.Email)
                ,new SqlParameter("@NewPhone", product.Phone)
                ,new SqlParameter("@NewName", product.Name)
                ,new SqlParameter("@NewAddress", product.Address)
                ,new SqlParameter("@NewCity", product.City)
                ,new SqlParameter("@NewState", product.State)
                ,new SqlParameter("@NewZip", product.Zip)
                
            };
            dataWriter.Write(parameters, "UpdatePurchaseById");
        }

        public List<PurchaseDM> GetPurchase()
        {
            return Read(null, "GetPurchase");
        }

        public PurchaseDM GetPurchaseById(int id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            return Read(parameters, "GetPurchaseById").SingleOrDefault();
        }

        public void CheckOut(InventoryDM product, string id, string total)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Total",total)
                ,new SqlParameter("@UserId", id) 
                ,new SqlParameter("@Stain",product.Stain)
                ,new SqlParameter("@Project", product.Project)
            };
            dataWriter.Write(parameters, "CreatePurchase");
        }
    }
}
