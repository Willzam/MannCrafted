﻿using ApplicationLogger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ProjectDAO:IProjectDAO
    {
        private ISQLDAO dataWriter;
        private ILoggerIO logs;
        private string connectionString = @"Server=.\SQLEXPRESS;Database = MannCrafted;Trusted_Connection=True;";

        public ProjectDAO(ILoggerIO log)
        {
            logs = log;
        }
        public void GetDataWriter(ISQLDAO dataWriter)
        {
            this.dataWriter = dataWriter;
        }
        public List<ProjectDM> Read(SqlParameter[] parameters, string statement)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(statement, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    connection.Open();
                    SqlDataReader data = command.ExecuteReader();
                    List<ProjectDM> projects = new List<ProjectDM>();
                    while (data.Read())
                    {
                        ProjectDM project = new ProjectDM();
                        project.Id = data["Id"].ToString();
                        project.Project = data["Project"].ToString();
                        projects.Add(project);
                    }
                    return projects;
                }
            }
        }
        public List<ProjectDM> GetProjects()
        {
            return Read(null, "GetProjects");
        }

        public void CreateProject(ProjectDM project)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Project", project.Project)
            };
            dataWriter.Write(parameters, "CreateProject");
            logs.LogError("Event", "A Project has been added to the database", "Class:ProjectDAO, Method:CreateProject");
        }

        public void DeleteProjectById(int id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            dataWriter.Write(parameters, "DeleteProjectById");
            logs.LogError("Event", "A Project has been removed from the database", "Class:ProjectDAO, Method:DeleteProjectById");

        }

        public void UpdateProjectById(ProjectDM project)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@NewProject", project.Project)
                , new SqlParameter("@Id", project.Id) 
            };
            dataWriter.Write(parameters, "UpdateProjectById");
            logs.LogError("Event", "A Project has been updated", "Class:ProjectDAO, Method: UpdateProjectById");
        }

        public ProjectDM GetProjectById(string id)
        {
            SqlParameter[] parameters = new SqlParameter[]{
                new SqlParameter("@Id", id)
            };
            return Read(parameters, "GetProjectById").SingleOrDefault();
        }
    }
}
