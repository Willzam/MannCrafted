﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BLL;

namespace MannCraftedV2.Models
{
    public class UserVM
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "User Name")]
        [StringLength(50, MinimumLength = 3)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(32, MinimumLength = 7)]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$", ErrorMessage = "You must have at least one upper case and one Special Character")]
        public string Password { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }

        public static UserSM Map(UserVM user)
        {
            UserSM people = new UserSM();
            people.Id = user.Id;
            people.UserName = user.UserName;
            people.Password = user.Password;
            people.Email = user.Email;
            people.Role = user.Role;
            people.Name = user.Name;
            people.Address = user.Address;
            people.City = user.City;
            people.State = user.State;
            people.Zip = user.Zip;
            people.Phone = user.Phone;
            return people;
        }
        public static UserVM Map(UserSM user)
        {
            UserVM people = new UserVM();
            people.Id = user.Id;
            people.UserName = user.UserName;
            people.Password = user.Password;
            people.Email = user.Email;
            people.Role = user.Role;
            people.Name = user.Name;
            people.Address = user.Address;
            people.City = user.City;
            people.State = user.State;
            people.Zip = user.Zip;
            people.Phone = user.Phone;
            return people;
        }
        public static List<UserSM> Map(List<UserVM> usersList)
        {
            List<UserSM> userlist = new List<UserSM>();
            foreach (UserVM user in usersList)
            {
                userlist.Add(Map(user));
            }
            return userlist;
        }
        public static List<UserVM> Map(List<UserSM> usersList)
        {
            List<UserVM> userlist = new List<UserVM>();
            foreach (UserSM user in usersList)
            {
                userlist.Add(Map(user));
            }
            return userlist;
        }
    }
}