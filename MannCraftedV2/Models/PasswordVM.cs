﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MannCraftedV2.Models
{
    public class PasswordVM
    {
        [Required]
        [Display(Name = "OldPassword")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [Display(Name = "NewPassword")]
        [StringLength(32, MinimumLength = 7)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required]
        [Display(Name = "ConfirmPassword")]
        [StringLength(32, MinimumLength = 7)]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        public int Id { get; set; }

        public static PasswordSM Map(PasswordVM pass)
        {
            PasswordSM recieve = new PasswordSM(); //Map for use on the Logic Layer
            recieve.Id = pass.Id;
            recieve.OldPassword = pass.OldPassword;
            recieve.NewPassword = pass.NewPassword;
            recieve.ConfirmPassword = pass.ConfirmPassword;
            return recieve;
        }
    }
}