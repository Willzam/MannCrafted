﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using BLL;
using ApplicationLogger;
using MannCraftedV2.Models;

namespace MannCraftedV2.Controllers
{
    public class UserController : Controller
    {
        public IUserLogic userLogic = new UserLogic(new UserDAO(new LoggerIO()), new SQLDAO(), new LoggerIO(), new Hashing());

        // GET: User
        public ActionResult Index()
        {
            List<UserVM> users = UserVM.Map(userLogic.GetUsers());
            return View(users);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            UserVM user = UserVM.Map(userLogic.GetUserById(id));
            ViewBag.Id = user.Id;
            ViewBag.UserName = user.UserName;
            ViewBag.Role = user.Role;
            ViewBag.Name = user.Name;
            ViewBag.Address = user.Address;
            ViewBag.City = user.City;
            ViewBag.State = user.State;
            ViewBag.Zip = user.Zip;
            ViewBag.Phone = user.Phone;
            return View(user);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(UserVM user)
        {
            try
            {
                // TODO: Add insert logic here
                userLogic.CreateUser(UserVM.Map(user));
                return RedirectToAction("Login");
            }
            catch (Exception Ucc)
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            UserVM user = UserVM.Map(userLogic.GetUserById(id));
            Session["EditId"] = user.Id;
            return View(user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, UserVM user)
        {
            try
            {
                // TODO: Add update logic here
                user.Password = UserVM.Map(userLogic.GetUserById(user.Id)).Password;
                userLogic.UpdateUserById(UserVM.Map(user));
                return RedirectToAction("Index");
            }
            catch (Exception Uce)
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            UserVM user = UserVM.Map(userLogic.GetUserById(id));
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(UserVM user)
        {
            try
            {
                // TODO: Add delete logic here
                userLogic.DeleteUserById(user.Id);
                return RedirectToAction("Index");
            }
            catch (Exception Ucd)
            {
                return View();
            }
        }
        // GET: User/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: User/Login
        [HttpPost]
        public ActionResult Login(UserVM user)
        {
            try
            {
                UserVM tempUser = UserVM.Map(userLogic.GetUser(UserVM.Map(user)));
                if (userLogic.Login(UserVM.Map(tempUser)))
                {
                    Session["UserId"] = tempUser.Id;
                    Session["UserName"] = tempUser.UserName;
                    Session["UserRole"] = tempUser.Role;
                    ViewBag.User = tempUser.UserName;
                    if (tempUser.Role == "User")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else if (tempUser.Role == "Power User")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else if (tempUser.Role == "Admin")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                return View();
            }
            catch (Exception UcLn)
            {
                return View();
            }
        }

        // GET: User/Login
        public ActionResult LogOut()
        {
            Session["UserId"] = null;
            return RedirectToAction("Index", "User");
        }

        //GET:  User/Profile
        public ActionResult Profile()
        {
            UserVM usuario = UserVM.Map(userLogic.GetUserById((int)Session["UserId"]));
            return View(usuario);
        }

        //GET: User/UpdatePassword
        public ActionResult UpdatePassword()
        {
            PasswordVM pass = new PasswordVM();
            pass.Id = (int)Session["EditId"];
            return View(pass);
        }

        //POST: User/UpdatePassword
        [HttpPost]
        public ActionResult UpdatePassword(PasswordVM pass)
        {
            try
            {
                userLogic.UpdatePassword(PasswordVM.Map(pass));
                return RedirectToAction("Index");
            }
            catch (Exception UcUp)
            {
                return View();
            }
        }
    }
}
