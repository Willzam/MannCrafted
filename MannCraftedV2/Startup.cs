﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MannCraftedV2.Startup))]
namespace MannCraftedV2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
